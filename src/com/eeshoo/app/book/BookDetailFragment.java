package com.eeshoo.app.book;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.db.dao.BookDAO;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.ConnectionDetector;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;
import com.skytree.epub.BookInformation;
import com.skytree.epubtest.BookViewActivity;
import com.skytree.epubtest.ContentHandler;
import com.skytree.epubtest.MagazineActivity;
import com.skytree.epubtest.SkySetting;
import com.skytree.epubtest.Unzip;
import com.squareup.picasso.Picasso;

public class BookDetailFragment extends AbstractBaseFragment implements
		OnClickListener {

	public static final String TAG = BookCategoryFragment.class.getName();
	public static final String ARG_ID = "pro_id";
	public static final String KEY = "key";
	public static String FILE;
	public static String BOOK;

	public String productId, key;

	TextView title, yearOfPub, author, price, desc, isbn, author_view;
	ImageView imageBook;
	Button btnBuyReadButton;
	BookDAO bookDao;
	BookDetail bookdetail;
	SharedPreferences sharedpreferences;

	ConnectionDetector cd;
	static Boolean isInternetPresent = false;

	ProgressDialog mProgressDialog;

	public static BookDetailFragment create(String id, String key) {
		BookDetailFragment fragment = new BookDetailFragment();
		Bundle args = new Bundle();
		args.putString(ARG_ID, id);
		args.putString(KEY, key);
		fragment.setArguments(args);
		return fragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedpreferences = getActivity().getSharedPreferences(
				AppConstants.EESHOO, Context.MODE_PRIVATE);
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		bookDao = new BookDAO(getActivity());
		if (getArguments() != null) {
			productId = getArguments().getString(ARG_ID);
			key = getArguments().getString(KEY);
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_layout_book_detail,
				null);
		if (((BaseFragment) getParentFragment()).mActivity.iconSearch.isShown()) {
			((BaseFragment) getParentFragment()).mActivity.iconSearch
					.setVisibility(View.GONE);
		}

		title = (TextView) rootView.findViewById(R.id.title);
		yearOfPub = (TextView) rootView.findViewById(R.id.year_of_pub);
		author = (TextView) rootView.findViewById(R.id.author);
		author_view = (TextView) rootView.findViewById(R.id.author_change);
		price = (TextView) rootView.findViewById(R.id.price);
		isbn = (TextView) rootView.findViewById(R.id.isbn);
		desc = (TextView) rootView.findViewById(R.id.synopsis_desc);
		imageBook = (ImageView) rootView.findViewById(R.id.image_book);

		btnBuyReadButton = (Button) rootView.findViewById(R.id.btn_buy_book);
		btnBuyReadButton.setOnClickListener(this);
		if (key.equals(AppConstants.READ)) {
			((BaseFragment) getParentFragment()).mActivity.tvTitle
					.setText(getString(R.string.read_book));
		} else {
			btnBuyReadButton.setText(getString(R.string.button_buy));
			((BaseFragment) getParentFragment()).mActivity.tvTitle
					.setText(getString(R.string.buy_book));
		}

		init();

		return rootView;
	}

	private void init() {
		if (key.equals(AppConstants.READ)) {
			bookdetail = getBookIfExist();
			if (bookdetail != null) {
				if (bookdetail.getResourcePath() != null) {
					updateUi(bookdetail);
					btnBuyReadButton.setText(getString(R.string.button_read));
				} else {
					btnBuyReadButton
							.setText(getString(R.string.button_download));
					callBookViewService();
				}
			} else {
				callBookViewService();
			}

		} else if (key.equals(AppConstants.BUY)) {
			callBookViewService();
		}

	}

	private void callBookViewService() {
		if (isInternetPresent) {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put(ARG_ID, productId);
			makeNetworkRequest(Web.PRODUCT_VIEW, Method.POST, params);
		} else {
			showAlertDialog(getActivity(),
					getString(R.string.valid_internet_permision),
					getString(R.string.valid_no_internet));
		}
	}

	private BookDetail getBookIfExist() {
		BookDetail tempBookDetail = null;
		if (sharedpreferences != null
				&& sharedpreferences.contains("customer_id")) {
			String userId = sharedpreferences.getString("customer_id", "");

			tempBookDetail = bookDao.getUserBookDetail(userId, productId);

		}

		return tempBookDetail;
	}

	@Override
	public void onResume() {
		super.onResume();
		setHeaderIcon(AppConstants.KEY_NONE);
	}

	protected void parseResponse(String request, String response) {
		UiWidget.hideProgressDialog();

		if (key.equals(AppConstants.READ)) {
			BookDetail tempBookDetail = JsonParser.getBookDetail(response);
			if (tempBookDetail.getAuthor() != null) {
				bookdetail.setAuthor(tempBookDetail.getAuthor());
			}
			if (tempBookDetail.getIsbn() != null) {
				bookdetail.setIsbn(tempBookDetail.getIsbn());
			}
			if (tempBookDetail.getName() != null) {
				bookdetail.setName((tempBookDetail.getName()));
			}
			if (tempBookDetail.getDescription() != null) {
				bookdetail.setDescription((tempBookDetail.getDescription()));
			}
			if (tempBookDetail.getPath() != null) {
				bookdetail.setPath(tempBookDetail.getPath());
			}
			if (tempBookDetail.getPrice() != null) {
				bookdetail.setPrice(tempBookDetail.getPrice());
			}
			if (tempBookDetail.getPublishDate() != null) {
				bookdetail.setPublishDate(tempBookDetail.getPublishDate());
			}
			updateUi(bookdetail);
		} else {
			bookdetail = JsonParser.getBookDetail(response);
			updateUi(bookdetail);
		}
	}

	private void downloadBookProcess(BookDetail bookdetail) {

		// declare the dialog as a member field of your activity

		// instantiate it within the onCreate method
		String urlEncode = null;
		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage(getResources().getString(
				R.string.downloading_in_progress));
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setCancelable(false);

		try {
			urlEncode = URLEncoder.encode(bookdetail.getName(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url = WSConstant.Web.DOWNLOAD + urlEncode + AppConstants.EXT;

		FILE = String.valueOf("book_" + bookdetail.getUserId() + productId
				+ AppConstants.EXT);

		final DownloadTask downloadTask = new DownloadTask(getActivity());
		downloadTask.execute(url, FILE);

	}

	private class DownloadTask extends AsyncTask<String, Integer, String> {

		private Context context;
		private PowerManager.WakeLock mWakeLock;

		public DownloadTask(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// take CPU lock to prevent CPU from going off if the user
			// presses the power button during download
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					getClass().getName());
			mWakeLock.acquire();
			mProgressDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);
			// if we get here, length is known, now set indeterminate to false
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setMax(100);
			mProgressDialog.setProgress(progress[0]);
		}

		@Override
		protected String doInBackground(String... params) {
			int count;
			InputStream input = null;
			OutputStream output = null;
			URLConnection connection = null;
			try {

				URL url = new URL(params[0]);
				connection = url.openConnection();
				connection.setConnectTimeout(30000);
				connection.connect();
				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				input = new BufferedInputStream(url.openStream(), 8192);

				makeDirectory(AppConstants.DIR);
				String filePath = Environment.getExternalStorageDirectory()
						.getAbsolutePath()
						+ "/"
						+ AppConstants.DIR
						+ "/"
						+ params[1];

				if (fileExists(filePath)) {
					deleteFile(filePath);
					createFile(filePath);
				} else {
					createFile(filePath);

				}

				// Output stream to write file
				output = new FileOutputStream(filePath);

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					// if (isCancelled()) {
					// input.close();
					// return null;
					// }
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress((int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams

			} catch (Exception e) {
				e.printStackTrace();
				return e.toString();
			} finally {
				try {
					if (output != null)
						output.close();
					if (input != null)
						input.close();
				} catch (IOException ignored) {
				}

			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			mWakeLock.release();
			mProgressDialog.dismiss();
			if (result == null) {
				String downloadedBookpath = Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ "/"
						+ AppConstants.DIR + "/" + FILE;

				updateBookIntoTable(downloadedBookpath);
			} else {
				Log.e("Error", result);
			}

		}

		public boolean makeDirectory(String dirName) {
			boolean res;
			String filePath = new String(
					Environment.getExternalStorageDirectory() + "/" + dirName);
			// String filePath = new String(getFilesDir().getAbsolutePath() +
			// "/"
			// + dirName);
			File file = new File(filePath);
			if (!file.exists()) {
				res = file.mkdirs();
			} else {

				res = false;
			}
			return res;
		}

		public boolean deleteFile(String fileName) {
			boolean res;
			File file = new File(fileName);
			// File file = new File(getFilesDir() + "/downloads/" + fileName);
			res = file.delete();
			return res;
		}

		public boolean createFile(String fileName) {
			boolean res;
			File file = new File(fileName);
			if (file.exists())
				res = true;
			else
				res = false;
			return res;
		}

		public boolean fileExists(String fileName) {
			boolean res;
			File file = new File(fileName);
			// File file = new File(getFilesDir() + "/downloads/" + fileName);

			if (file.exists())
				res = true;
			else
				res = false;
			return res;
		}

	}

	private void updateBookIntoTable(String path) {

		// bookdetail.setUserId(userId);
		if (path != null) {
			bookdetail.setResourcePath(path);
			new UpdateBookDetailTask().execute(bookdetail);
		}
		// bookdetail.setProductId(productId);

	}

	private void getBookDetailFromTable() {

		if (sharedpreferences != null
				&& sharedpreferences.contains("customer_id")) {
			String userId = sharedpreferences.getString("customer_id", "");
			new GetBookDetailTask().execute(userId, productId);
		}

	}

	private void updateUi(BookDetail bookdetail) {
		this.bookdetail = bookdetail;
		Picasso.with(getActivity()).load(bookdetail.getImage()).into(imageBook);
		if (bookdetail.getName() != null) {
			String titleValue = (bookdetail.getName().equals("N/A")) ? ""
					: bookdetail.getName();
			title.setText(titleValue);
		}
		if (bookdetail.getPublishDate() != null) {
			String pub_year = (bookdetail.getPublishDate().equals("N/A")) ? ""
					: bookdetail.getAuthor();
			yearOfPub.setText(pub_year);
		}
		if (bookdetail.getAuthor() != null) {
			// String mStringNew=
			// getActivity().getResources().getString(R.string.new_change);
			// String textAuthor = (bookdetail.getAuthor().equals("N/A") ||
			// bookdetail
			// .getAuthor().equals("")) ? "" : "By:-"
			// + bookdetail.getAuthor();

			if ((bookdetail.getAuthor().equals("N/A") || bookdetail.getAuthor()
					.equals(""))) {
				author.setText("");
				author_view.setVisibility(View.INVISIBLE);
			} else {
				author.setText(bookdetail.getAuthor());
				author_view.setVisibility(View.VISIBLE);

			}
		} else {
			author.setText("");
			author_view.setVisibility(View.INVISIBLE);
		}

		if (bookdetail.getIsbn() != null) {
			String isbn_string = (bookdetail.getIsbn().equals("N/A")) ? ""
					: bookdetail.getIsbn();
			isbn.setText(isbn_string);
		}
		if (bookdetail.getPrice() != null) {
			String priceValue = (bookdetail.getPrice().equals("N/A")) ? ""
					: bookdetail.getPrice();
			price.setText(priceValue);
		}
		if (bookdetail.getDescription() != null) {
			String description = (bookdetail.getDescription().equals("N/A")) ? ""
					: bookdetail.getDescription();
			desc.setText(description);
		}

	}

	public class UpdateBookDetailTask extends AsyncTask<BookDetail, Void, Long> {

		@Override
		protected Long doInBackground(BookDetail... params) {
			return bookDao.updateBookDetail(params[0]);
		}

		@Override
		protected void onPostExecute(Long result) {
			if (result != 0) {
				btnBuyReadButton.setText(getString(R.string.button_read));
			} else {
				bookdetail.setResourcePath(null);
			}
		}

	}

	public class GetBookDetailTask extends AsyncTask<String, Void, BookDetail> {

		@Override
		protected BookDetail doInBackground(String... params) {
			return bookDao.getUserBookDetail(params[0], params[1]);
		}

		@Override
		protected void onPostExecute(BookDetail bookdetail) {
			updateUi(bookdetail);
		}

	}

	/**
	 * all network request using this method
	 * 
	 * @param request
	 *            url
	 * @param method
	 *            get/post
	 * @param rParams
	 *            request params
	 * 
	 */

	private void makeNetworkRequest(final String request, int method,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + request;
		UiWidget.showProgressDialog(getActivity());

		StringRequest strReq = new StringRequest(method, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						parseResponse(request, response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
						getActivity().onBackPressed();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.btn_buy_book:
			if (key.equals(AppConstants.READ)) {
				if (bookdetail != null) {
					if (bookdetail.getResourcePath() != null) {
						initiateEpubReader();
					} else {
						if (isInternetPresent) {
							downloadBookProcess(bookdetail);
						} else {
							UiWidget.showAlertDialog(
									getActivity(),
									getString(R.string.valid_internet_permision),
									getString(R.string.valid_no_internet));
						}
					}
				} else {
					UiWidget.showAlertDialog(getActivity(),
							getString(R.string.valid_error),
							getString(R.string.valid_no_resource));
				}

			} else if (key.equals(AppConstants.BUY)) {

				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(bookdetail.getPath().trim()));
				startActivity(i);
			}

			break;

		default:
			break;
		}

	}

	private void initiateEpubReader() {

		if (SkySetting.getStorageDirectory() == null) {
			// All book related data will be stored
			// /data/data/com....../files/appName/
			// SkySetting.setStorageDirectory(getFilesDir().getAbsolutePath(),
			// appName);
			// All book related data will be stored /sdcard/appName/...
			SkySetting.setStorageDirectory(Environment
					.getExternalStorageDirectory().getAbsolutePath(),
					AppConstants.DIR);
		}

		if (bookdetail.getResourcePath() != null) {
			File bookPath = new File(bookdetail.getResourcePath());
			if (bookPath.exists()) {
				String bookName = bookPath.getName();
				if (sharedpreferences != null
						&& sharedpreferences.contains(bookName)
						&& sharedpreferences.getBoolean(bookName, false)) {
					startBookView(
							bookName,
							bookdetail.getAuthor(),
							bookdetail.getName(),
							String.valueOf(bookdetail.getUserId()
									+ bookdetail.getProductId()));
				} else {
					if (installSamples(bookName)) {
						bookdetail.setIsReading(1);
						new UpdateBookDetailTask().execute(bookdetail);
						startBookView(
								bookName,
								bookdetail.getAuthor(),
								bookdetail.getName(),
								String.valueOf(bookdetail.getUserId()
										+ bookdetail.getProductId()));

					}

				}

			} else {
				// Toast.makeText(getActivity(), "No resource Found",
				// Toast.LENGTH_SHORT).show();
				UiWidget.showAlertDialog(getActivity(),
						getString(R.string.valid_error),
						getString(R.string.valid_no_resource));
			}
		} else {
			UiWidget.showAlertDialog(getActivity(),
					getString(R.string.valid_error),
					getString(R.string.valid_no_resource));

		}

	}

	private boolean installSamples(String bookName) {
		// if (this.isSampleInstalled()) return;

		boolean status = false;
		try {

			if (!this.makeDirectory("scripts")) {
				debug("faild to make scripts directory");
			}

			if (!this.makeDirectory("images")) {
				debug("faild to make images directory");
			}

			copyFileToFolder("PagesCenter.png", "images");
			copyFileToFolder("PagesStack.png", "images");
			copyFileToFolder("Pad-Landscape.png", "images");
			copyFileToFolder("Pad-Portrait.png", "images");
			copyFileToFolder("Phone-Landscape.png", "images");
			copyFileToFolder("Phone-Landscape-Black.png", "images");
			copyFileToFolder("Phone-Landscape-Brown.png", "images");
			copyFileToFolder("Phone-Landscape-White.png", "images");
			copyFileToFolder("Phone-Portrait.png", "images");
			copyFileToFolder("Phone-Portrait-Black.png", "images");
			copyFileToFolder("Phone-Portrait-Brown.png", "images");
			copyFileToFolder("Phone-Portrait-White.png", "images");

			if (!this.makeDirectory("downloads")) {
				debug("faild to make downloads directory");
			}
			//
			if (!this.makeDirectory("books")) {
				debug("faild to make books directory");
			}

			// this.installBook("Mobydick.epub");
			this.installBook(bookName);
			// this.installBook(bookdetail.getResourcePath());
			// this.installBook(String.valueOf(bookdetail.getName() +
			// AppConstants.EXT));

			SharedPreferences.Editor edit = sharedpreferences.edit();

			edit.putBoolean(bookName, true);

			edit.commit();

			status = true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;

		// startBookView2("sample.epub");

	}

	private void startBookView2(String file) {
		// TODO Auto-generated method stub
		String baseDirectory = SkySetting.getStorageDirectory() + "/books";
		ContentHandler contentListener = new ContentHandler();
		BookInformation bi = new BookInformation(file, baseDirectory,
				contentListener);
		debug(bi.creator);
		Intent intent;
		if (!bi.isFixedLayout) {
			intent = new Intent(getActivity(), BookViewActivity.class);
		} else {
			intent = new Intent(getActivity(), MagazineActivity.class);
		}
		intent.putExtra("BOOKNAME", file);
		intent.putExtra("AUTHOR", "Test");
		intent.putExtra("TITLE", "test");
		intent.putExtra("BOOKCODE", 123);

		intent.putExtra("transitionType", 2);
		startActivity(intent);
	}

	private void startBookView(String fileName, String author, String title,
			String code) {
		// String baseDirectory = getFilesDir() + "/books";
		String baseDirectory = SkySetting.getStorageDirectory() + "/books";
		ContentHandler contentListener = new ContentHandler();
		BookInformation bi = new BookInformation(fileName, baseDirectory,
				contentListener);
		debug(bi.creator);
		Intent intent;
		if (!bi.isFixedLayout) {
			intent = new Intent(getActivity(), BookViewActivity.class);
		} else {
			intent = new Intent(getActivity(), MagazineActivity.class);
		}
		intent.putExtra("BOOKNAME", fileName);
		intent.putExtra("AUTHOR", author);
		intent.putExtra("TITLE", title);
		intent.putExtra("BOOKCODE", Integer.parseInt(code));

		intent.putExtra("transitionType", 1);
		startActivity(intent);
	}

	private void installBook(String fileName) {
		if (this.fileExists(fileName)) {
			Log.d(TAG, fileName + " already exist. try to delete old file.");
			this.deleteFile(fileName);
		}
		this.copyToDevice(fileName);
		this.unzipBook(fileName);
	}

	public boolean fileExists(String fileName) {
		boolean res;
		File file = new File(SkySetting.getStorageDirectory() + "/downloads/"
				+ fileName);
		// File file = new File(getFilesDir() + "/downloads/" + fileName);
		debug(file.getAbsolutePath());

		if (file.exists())
			res = true;
		else
			res = false;
		return res;
	}

	public boolean deleteFile(String fileName) {
		boolean res;
		File file = new File(SkySetting.getStorageDirectory() + "/downloads/"
				+ fileName);
		// File file = new File(getFilesDir() + "/downloads/" + fileName);
		res = file.delete();
		return res;
	}

	public void unzipBook(String fileName) {
		String targetDir = new String(SkySetting.getStorageDirectory()
				+ "/books/" + fileName);
		// String targetDir = new String(getFilesDir().getAbsolutePath()
		// + "/books/" + fileName);
		targetDir = this.removeExtention(targetDir);
		String filePath = new String(SkySetting.getStorageDirectory()
				+ "/downloads/");
		// String filePath = new String(getFilesDir().getAbsolutePath()
		// + "/downloads/");

		Unzip unzip = new Unzip(fileName, filePath, targetDir);
		unzip.unzip();
	}

	public String removeExtention(String filePath) {
		// These first few lines the same as Justin's
		File f = new File(filePath);

		// if it's a directory, don't remove the extention
		if (f.isDirectory())
			return filePath;

		String name = f.getName();

		// Now we know it's a file - don't need to do any special hidden
		// checking or contains() checking because of:
		final int lastPeriodPos = name.lastIndexOf('.');
		if (lastPeriodPos <= 0) {
			// No period after first character - return name as it was passed in
			return filePath;
		} else {
			// Remove the last period and everything after it
			File renamed = new File(f.getParent(), name.substring(0,
					lastPeriodPos));
			return renamed.getPath();
		}
	}

	public void debug(String msg) {
		// if (Setting.isDebug()) {
		Log.d("EPub", msg);
		// }
	}

	public boolean makeDirectory(String dirName) {
		boolean res;
		String filePath = new String(SkySetting.getStorageDirectory() + "/"
				+ dirName);
		// String filePath = new String(getFilesDir().getAbsolutePath() + "/"
		// + dirName);
		debug(filePath);
		File file = new File(filePath);
		if (!file.exists()) {
			res = file.mkdirs();
		} else {
			res = false;
		}
		return res;
	}

	public void copyToDevice(String fileName) {
		if (!this.fileExists(fileName)) {
			try {
				File file = new File(SkySetting.getStorageDirectory() + "/"
						+ fileName);
				InputStream localInputStream = new FileInputStream(file);
				// InputStream localInputStream =
				// getActivity().getAssets().open(
				// fileName);
				// FileOutputStream localFileOutputStream = new
				// FileOutputStream(
				// SkySetting.getStorageDirectory() + "/downloads/"
				// + file.getName());
				FileOutputStream localFileOutputStream = new FileOutputStream(
						SkySetting.getStorageDirectory() + "/downloads/"
								+ fileName);

				byte[] arrayOfByte = new byte[1024];
				int offset;
				while ((offset = localInputStream.read(arrayOfByte)) > 0) {
					localFileOutputStream.write(arrayOfByte, 0, offset);
				}
				localFileOutputStream.close();
				localInputStream.close();
				Log.d(TAG, fileName + " copied to phone");
			} catch (IOException localIOException) {
				localIOException.printStackTrace();
				Log.d(TAG, "failed to copy");
				return;
			}
		} else {
			Log.d(TAG, fileName + " already exist");
		}
	}

	public void copyFileToFolder(String fileName, String folder) {
		// if (!this.fileExists(fileName)){
		try {
			InputStream localInputStream = getActivity().getAssets().open(
					fileName);
			FileOutputStream localFileOutputStream = new FileOutputStream(
					SkySetting.getStorageDirectory() + "/" + folder + "/"
							+ fileName);
			// FileOutputStream localFileOutputStream = new FileOutputStream(
			// getFilesDir().getAbsolutePath() + "/" + folder + "/"
			// + fileName);

			byte[] arrayOfByte = new byte[1024];
			int offset;
			while ((offset = localInputStream.read(arrayOfByte)) > 0) {
				localFileOutputStream.write(arrayOfByte, 0, offset);
			}
			localFileOutputStream.close();
			localInputStream.close();
			Log.d(TAG, fileName + " copied to phone");
		} catch (IOException localIOException) {
			localIOException.printStackTrace();
			Log.d(TAG, "failed to copy");
			return;
		}
		// }
		// else {
		// Log.d(TAG, fileName+" already exist");
		// }
	}

}
