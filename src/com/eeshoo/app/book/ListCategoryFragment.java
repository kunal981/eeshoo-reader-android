package com.eeshoo.app.book;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.model.BookCategory;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;

public class ListCategoryFragment extends AbstractBaseFragment {

	public static final String TAG = ListCategoryFragment.class.getName();

	ListView listOfBook;

	CustomListAdapter adapter;
	List<BookCategory> listBookCategories;

	String[] categories = { "Featured", "Shorts Read",
			"Contemporary Literature", "Fantasy & Science Fiction", "Romance",
			"Classic Literture", "History & Culture", "Ancient Books",
			"Poetry,Plays & lyrics", "Art & Music", "Philisophy & Religion",
			"Psychology", "Social Science", "Politics & Military", "Biogarphy",
			"Business", "Self-Improvement", "Daily Guidence", "Travel",
			"Medicine & Health", "Lifestyle", "Cookbooks", "Sports & Fitness",
			"Education & Training", "Children's", "Computer & Internet",
			"Science & Technology", "Reference", "Magazine & Comics",
			"Audio books" };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listBookCategories = new ArrayList<BookCategory>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_layout_book_category, null);
		((BaseFragment) getParentFragment()).mActivity.tvTitle
				.setText(R.string.menu_bar_title_categories);
		if (((BaseFragment) getParentFragment()).mActivity.iconSearch.isShown()) {
			((BaseFragment) getParentFragment()).mActivity.iconSearch
					.setVisibility(View.GONE);
		}

		listOfBook = (ListView) rootView.findViewById(R.id.list_of_book);
		adapter = new CustomListAdapter(getActivity(), listBookCategories);
		listOfBook.setAdapter(adapter);
		if (listBookCategories.size() == 0)
			init();

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		setHeaderIcon(AppConstants.KEY_NONE);
	}

	private void init() {
		HashMap<String, String> params = new HashMap<String, String>();
		makeNetworkRequest(Web.CATEGORIES, Method.GET, params);

	}

	protected void parseResponse(String request, String response) {
		UiWidget.hideProgressDialog();

		if (request.equals(Web.CATEGORIES)) {
			listBookCategories = JsonParser.getAllCategory(response);
			adapter = new CustomListAdapter(getActivity(), listBookCategories);
			listOfBook.setAdapter(adapter);
			addListViewlistener();
		}

	}

	// ListView itemclickListener
	private void addListViewlistener() {
		listOfBook.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String categroryName = listBookCategories.get(position)
						.getName();
				String categoryId = String.valueOf(listBookCategories.get(
						position).getId());
				final BookCategoryFragment bookCategory = BookCategoryFragment
						.create(categroryName, categoryId);

				String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
						.getCurrentTabTag();
				Log.i(TAG, "Current Tab:" + tag);
				((BaseFragment) getParentFragment()).pushFragments(tag,
						bookCategory, true, true);

			}

		});

	}

	/*
	 * add all custom adapter here
	 */

	class CustomListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		List<BookCategory> listBookCategories;

		public CustomListAdapter(Activity activity,
				List<BookCategory> listBookCategories) {
			this.activity = activity;
			this.listBookCategories = listBookCategories;
		}

		@Override
		public int getCount() {

			return listBookCategories.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.layout_list_row_more_item, null);

				holder = new ViewHolder();
				holder.textView = (TextView) convertView
						.findViewById(R.id.categories_item);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			BookCategory bookCategory = listBookCategories.get(position);
			holder.textView.setText(bookCategory.getName());
			return convertView;

		}
	}

	static class ViewHolder {
		TextView textView;
	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeNetworkRequest(final String request, int method,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + request;
		UiWidget.showProgressDialog(getActivity());

		StringRequest strReq = new StringRequest(method, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						parseResponse(request, response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

}
