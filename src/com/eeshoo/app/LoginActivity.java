package com.eeshoo.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.eeshoo.app.R;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.db.dao.BookDAO;
import com.eeshoo.app.db.dao.UserDAO;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.model.UserDetail;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.ConnectionDetector;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.PARAM;
import com.eeshoo.app.util.WSConstant.Web;

public class LoginActivity extends ActionBarActivity {

	private static final String TAG = LoginActivity.class.getName();

	private static final int TAG_1 = 328;

	static Boolean isInternetPresent = false; // flag for Internet connection
												// status

	// Connection detector class
	ConnectionDetector cd;

	EditText editEmail, editPassword;
	TextView forgotPassword;
	Button buttonLogin;

	String email, password;

	SharedPreferences sharedpreferences;

	UserDetail userDetail;
	List<BookDetail> listBook;

	UserDAO userDao;
	BookDAO bookDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout_login);

		cd = new ConnectionDetector(getApplicationContext());
		userDao = new UserDAO(this);
		bookDao = new BookDAO(this);
		sharedpreferences = getSharedPreferences(AppConstants.EESHOO,
				Context.MODE_PRIVATE);
		isInternetPresent = cd.isConnectingToInternet();

		editEmail = (EditText) findViewById(R.id.email);
		editPassword = (EditText) findViewById(R.id.password);
		forgotPassword = (TextView) findViewById(R.id.text_forgot);

		buttonLogin = (Button) findViewById(R.id.login);

		buttonLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (cd.isConnectingToInternet()) {
					email = editEmail.getText().toString();
					password = editPassword.getText().toString();
					if (validateLogin()) {
						HashMap<String, String> params = new HashMap<String, String>();
						params.put(PARAM.EMAIL, email);
						params.put(PARAM.PWD, password);
						makeNetworkRequestForLogin(Web.LOGIN, params);
					}

				} else {
					checkInternet(cd.isConnectingToInternet());
				}

			}
		});

		forgotPassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this,
						ForgotActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
				finish();

			}
		});

		// checkInternet(isInternetPresent);

	}

	/**
	 * This method validate the email and password field not to be null
	 * 
	 * @return
	 */
	protected boolean validateLogin() {
		if (email.trim().equals("")) {
			showAlertDialog(LoginActivity.this,
					getString(R.string.valid_error),
					getString(R.string.valid_email));
			editEmail.requestFocus();
			return false;
		}
		if (password.trim().equals("")) {
			showAlertDialog(LoginActivity.this,
					getString(R.string.valid_error),
					getString(R.string.valid_password));
			editPassword.requestFocus();
			return false;
		}

		return true;
	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeNetworkRequestForLogin(String login,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + login;
		UiWidget.showProgressDialog(this);

		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						// msgResponse.setText(response.toS
						// string());
						parseResponse(response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	protected void parseResponse(String response) {
		UiWidget.hideProgressDialog();
		try {

			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {

				String userId = null;
				userDetail = new UserDetail();
				listBook = new ArrayList<BookDetail>();

				if (jsonOBject.has("customer_id")) {
					userId = jsonOBject.getString("customer_id");
					userDetail.setId(Long.parseLong(userId));
				}
				if (jsonOBject.has("name")) {
					userDetail.setFirstName(jsonOBject.getString("name"));
				}
				userDetail.setEmail(email);
				userDetail.setPassword(password);

				if (jsonOBject.has("orders")) {

					JSONArray jBookArrary = jsonOBject.getJSONArray("orders");
					for (int i = 0; i < jBookArrary.length(); i++) {
						JSONObject jObjBook = jBookArrary.getJSONObject(i);
						BookDetail detail = new BookDetail();
						detail.setProductId(jObjBook.getString("product_id"));
						detail.setName(jObjBook.getString("name"));
						detail.setAuthor(jObjBook.getString("author"));
						detail.setDescription(jObjBook.getString("description"));
						detail.setImage(jObjBook.getString("image"));
						detail.setUserId(userId);
						listBook.add(detail);
					}
				}

				new AddUserTask().execute(userDetail);

			} else {
				showAlertDialog(LoginActivity.this,
						getString(R.string.valid_error),
						getString(R.string.valid_email_password));

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			showAlertDialog(LoginActivity.this,
					getString(R.string.valid_error),
					getString(R.string.valid_email_password));
		}

	}

	public void addUserBookDetail() {
		if (listBook.size() != 0) {
			new AddUserBookDetailTask().execute((Void) null);
		} else {
			startActivityHome();
		}

	}

	private void startActivityHome() {
		Editor editor = sharedpreferences.edit();
		editor.putString("email", email);
		editor.putString("password", password);
		editor.putString("customer_id", String.valueOf(userDetail.getId()));
		editor.commit();

		Intent intent = new Intent(this, AppMainTabActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		finish();

	}

	/**
	 * async task for database operation
	 * 
	 */

	public class AddUserTask extends AsyncTask<UserDetail, Void, Long> {

		@Override
		protected Long doInBackground(UserDetail... params) {
			// userDao.save(params[0]);
			return userDao.save(params[0]);
		}

		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
			// Log.d(TAG, "user_id" + result);
			if (result != -1) {
				Log.e(TAG, "User added to database sucessfully");
				addUserBookDetail();
			}
		}

	}

	public class AddUserBookDetailTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			return bookDao.saveUserBookList(listBook);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			startActivityHome();
		}

	}

	/**
	 * 
	 * @param isInternetPresent2
	 *            boolean for internet state true for internet false for no
	 *            connection
	 */
	private void checkInternet(Boolean isInternetPresent2) {
		// check for Internet status
		if (!isInternetPresent2) {
			showAlertDialog(LoginActivity.this,
					getString(R.string.valid_internet_permision),
					getString(R.string.valid_no_internet));

		}

	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setIcon(R.drawable.fail);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void onButtonClick(View view) {

	}

}
