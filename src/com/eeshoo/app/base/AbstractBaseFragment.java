package com.eeshoo.app.base;

import com.eeshoo.app.util.AppConstants;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class AbstractBaseFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void setHeaderIcon(int key) {

		((BaseFragment) getParentFragment()).mActivity.iconSearch
				.setVisibility(View.GONE);
		((BaseFragment) getParentFragment()).mActivity.iconReferesh
				.setVisibility(View.GONE);
		((BaseFragment) getParentFragment()).mActivity.iconLogout
				.setVisibility(View.GONE);

		switch (key) {
		case AppConstants.KEY_SEARCH:
			((BaseFragment) getParentFragment()).mActivity.iconSearch
					.setVisibility(View.VISIBLE);
			break;
		case AppConstants.KEY_REFRESH:
			((BaseFragment) getParentFragment()).mActivity.iconReferesh
					.setVisibility(View.VISIBLE);
			break;
		case AppConstants.KEY_LOGOUT:
			((BaseFragment) getParentFragment()).mActivity.iconLogout
					.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

}
