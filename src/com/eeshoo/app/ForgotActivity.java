package com.eeshoo.app;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.eeshoo.app.R;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;

public class ForgotActivity extends ActionBarActivity {

	private static final String TAG = ForgotActivity.class.getName();

	private static final int TAG_1 = 328;

	EditText editEmail;
	Button buttonSubmit;

	String email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout_forgot_pwd);

		editEmail = (EditText) findViewById(R.id.email);
		buttonSubmit = (Button) findViewById(R.id.submit);

		buttonSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!editEmail.getText().toString().equals("")) {

					String email = editEmail.getText().toString().trim();
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("email", email);
					makeNetworkRequestForPassword(Web.PASSWORD, params);

				} else {
					UiWidget.showAlertDialog(ForgotActivity.this,
							getString(R.string.valid_error),
							getString(R.string.valid_email));
				}

			}
		});

		// checkInternet(isInternetPresent);

	}

	/**
	 * Making json object request to register user;
	 * */

	private void makeNetworkRequestForPassword(String password,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + password;
		UiWidget.showProgressDialog(this);

		StringRequest strReq = new StringRequest(Method.POST, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						// msgResponse.setText(response.toS
						// string());
						parseResponse(response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

	protected void parseResponse(String response) {
		UiWidget.hideProgressDialog();
		try {
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				showAlertDialog(ForgotActivity.this, "Forgot password",
						"Your password is changed. Please check your mail",
						false);
			} else {
				showAlertDialog(ForgotActivity.this, "Forgot password",
						"Password Change failed. Please try after some time",
						false);

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
						onBackPressed();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();

	}

}
