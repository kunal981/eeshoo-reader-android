package com.eeshoo.app.util;

import com.eeshoo.app.R;

public class AppConstants {

	public static final String EESHOO = "eeshoo-aap";
	public static final String TAB_HOME = "主页";
	public static final String TAB_EXPLORE = "浏览";
	public static final String TAB_PROFILE = "用户信息";
	public static final String READ = "read";
	public static final String BUY = "buy";
	public static final String EXT = ".epub";
	public static final String DIR = "Eeshoo";

	public static final int KEY_SEARCH = 1;
	public static final int KEY_REFRESH = 2;
	public static final int KEY_LOGOUT = 3;
	public static final int KEY_NONE = 0;

	public static final Integer[] testDrawableArrary = {
			R.drawable.test_book_4, R.drawable.test_book_5,
			R.drawable.test_book_6, R.drawable.test_book_7,
			R.drawable.test_book_8, R.drawable.test_book_9, };
	public static final Integer[] testDrawableArraryDouble = {
			R.drawable.test_book_4, R.drawable.test_book_5,
			R.drawable.test_book_6, R.drawable.test_book_7,
			R.drawable.test_book_8, R.drawable.test_book_9,
			R.drawable.test_book_4, R.drawable.test_book_5,
			R.drawable.test_book_6, R.drawable.test_book_7,
			R.drawable.test_book_8, R.drawable.test_book_9 };
	public static final Integer[] testDrawableArraryTiple = {
			R.drawable.menu_books_1, R.drawable.menu_books_2,
			R.drawable.menu_books_3, R.drawable.menu_books_1,
			R.drawable.menu_books_2, R.drawable.menu_books_3,
			R.drawable.menu_books_1, R.drawable.menu_books_2,
			R.drawable.menu_books_3, R.drawable.menu_books_1,
			R.drawable.menu_books_2, R.drawable.menu_books_3,
			R.drawable.menu_books_1, R.drawable.menu_books_2,
			R.drawable.menu_books_3, R.drawable.menu_books_1,
			R.drawable.menu_books_2, R.drawable.menu_books_3 };

}
