package com.eeshoo.app.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.eeshoo.app.R;
import com.eeshoo.app.base.AbstractBaseFragment;
import com.eeshoo.app.base.BaseFragment;
import com.eeshoo.app.book.BookDetailFragment;
import com.eeshoo.app.controller.AppController;
import com.eeshoo.app.model.BookDetail;
import com.eeshoo.app.parser.JsonParser;
import com.eeshoo.app.util.AppConstants;
import com.eeshoo.app.util.UiWidget;
import com.eeshoo.app.util.WSConstant;
import com.eeshoo.app.util.WSConstant.Web;
import com.squareup.picasso.Picasso;

public class SearchBook extends AbstractBaseFragment {

	public static final String TAG = SearchBook.class.getName();

	EditText editSearch;

	ListView listOfBook;
	List<BookDetail> listBookDetails;

	CustomListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listBookDetails = new ArrayList<BookDetail>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_search_book,
				null);

		((BaseFragment) getParentFragment()).mActivity.tvTitle
				.setText(getString(R.string.search_title));

		editSearch = (EditText) rootView.findViewById(R.id.search_text);
		addtextSearchListner();
		listOfBook = (ListView) rootView.findViewById(R.id.list_of_book);
		adapter = new CustomListAdapter(getActivity(), listBookDetails);
		listOfBook.setAdapter(adapter);
		addListViewlistener();
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		setHeaderIcon(AppConstants.KEY_NONE);
	}

	private void addtextSearchListner() {

		editSearch
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							if (!editSearch.getText().toString().trim()
									.equals("")) {
								HashMap<String, String> params = new HashMap<String, String>();
								params.put("name", editSearch.getText()
										.toString());
								makeNetworkRequest(Web.SEARCH, Method.POST,
										params);
								UiWidget.hideKeyboard(getActivity(), editSearch);

							} else {
								UiWidget.showAlertDialog(getActivity(),
										getString(R.string.valid_error),
										getString(R.string.valid_search));
							}
							return true;
						}
						return false;
					}
				});

	}

	protected void parseResponse(String request, String response) {
		UiWidget.hideProgressDialog();

		listBookDetails = JsonParser.getCategoryProduct(response);
		adapter.updateListItem(listBookDetails);

	}

	// ListView itemclickListener
	private void addListViewlistener() {
		listOfBook.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String productId = listBookDetails.get(position).getProductId();
				BookDetailFragment bookFragment = BookDetailFragment.create(
						productId, AppConstants.BUY);
				String tag = ((BaseFragment) getParentFragment()).mActivity.mTabHost
						.getCurrentTabTag();
				Log.i(TAG, "Current Tab:" + tag);
				((BaseFragment) getParentFragment()).pushFragments(tag,
						bookFragment, true, true);

			}

		});

	}

	/*
	 * add all custom adapter here
	 */

	class CustomListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		private List<BookDetail> listbookdetail;

		public CustomListAdapter(Activity activity,
				List<BookDetail> listbookdetail) {
			this.activity = activity;
			this.listbookdetail = listbookdetail;
		}

		public void updateListItem(List<BookDetail> listbookdetail) {
			this.listbookdetail = listbookdetail;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {

			return listbookdetail.size();
		}

		@Override
		public Object getItem(int position) {

			return listbookdetail.get(position);
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;
			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_list_book_item,
						null);
				holder = new ViewHolder();
				holder.imageView = (ImageView) convertView
						.findViewById(R.id.image_book);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.desc = (TextView) convertView
						.findViewById(R.id.description);
				holder.author = (TextView) convertView
						.findViewById(R.id.author);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			BookDetail bookDetail = listbookdetail.get(position);
			Picasso.with(getActivity()).load(bookDetail.getImage())
					.into(holder.imageView);
			holder.title.setText(bookDetail.getName());
			holder.desc.setText(bookDetail.getDescription());
			holder.author.setText(bookDetail.getAuthor());
			return convertView;

		}
	}

	static class ViewHolder {
		ImageView imageView;
		TextView title;
		TextView desc;
		TextView author;

	}

	/**
	 * all network request using this method
	 * 
	 * @param request
	 *            url
	 * @param method
	 *            get/post
	 * @param rParams
	 *            request params
	 * 
	 */

	private void makeNetworkRequest(final String request, int method,
			final Map<String, String> rParams) {

		String url = WSConstant.Web.HOST + request;
		UiWidget.showProgressDialog(getActivity());

		StringRequest strReq = new StringRequest(method, url,
				new com.android.volley.Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						Log.d(TAG + "On Response", response);
						parseResponse(request, response);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						UiWidget.hideProgressDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = rParams;
				return params;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(strReq,
				WSConstant.PARAM.TAG_JSON_OBJECT);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

	}

}
