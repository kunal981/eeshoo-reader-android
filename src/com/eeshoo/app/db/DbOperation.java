package com.eeshoo.app.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DbOperation {

	private static String TAG = "DB_OPERATION";

	protected SQLiteDatabase database;
	private DbHelper dbHelper;
	private Context mContext;

	public DbOperation(Context context) {
		this.mContext = context;
		dbHelper = DbHelper.getHelper(mContext);
		open();
	}

	public void open() throws SQLException {
		if (dbHelper == null)
			dbHelper = DbHelper.getHelper(mContext);
		database = dbHelper.getWritableDatabase();
	}
}
