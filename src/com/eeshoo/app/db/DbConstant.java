package com.eeshoo.app.db;

import android.provider.BaseColumns;

public class DbConstant {

	public static abstract class User implements BaseColumns {

		public static final String USER_TABLE = "user";
		public static final String USER_FRIST_NAME = "user_first_name";
		public static final String USER_LAST_NAME = "user_last_name";
		public static final String EMAIL = "email";
		public static final String PASSWORD = "password";
		public static final String BOOK_ID = "book_id";

	}

	public static abstract class Book implements BaseColumns {

		public static final String TABLE = "user_book";
		public static final String PRODUCT_ID = "product_id";
		public static final String NAME = "name";
		public static final String AUTHOR = "author";
		public static final String DESC = "description";
		public static final String IMAGE = "image";
		public static final String USER_ID = "user_id";
		public static final String PRICE = "price";
		public static final String ISBN = "isbn";
		public static final String PUBLICATION = "pub_date";
		public static final String PATH = "path";
		public static final String RESOURCE_PATH = "resource_path";
		public static final String IS_READING = "is_reading";

	}

}
