package com.eeshoo.app.db.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.eeshoo.app.db.DbConstant.Book;
import com.eeshoo.app.db.DbConstant.User;
import com.eeshoo.app.db.DbOperation;
import com.eeshoo.app.model.BookDetail;

public class BookDAO extends DbOperation {

	private static final String WHERE_ID_EQUALS = Book._ID + " =?";

	public BookDAO(Context context) {
		super(context);
	}

	public long save(BookDetail bookDetail) {
		ContentValues values = new ContentValues();
		values.put(Book.PRODUCT_ID, bookDetail.getProductId());
		values.put(Book.NAME, bookDetail.getName());
		values.put(Book.AUTHOR, bookDetail.getAuthor());
		values.put(Book.DESC, bookDetail.getDescription());
		values.put(Book.IMAGE, bookDetail.getImage());
		values.put(Book.USER_ID, bookDetail.getUserId());
		values.put(Book.PRICE, bookDetail.getPrice());
		values.put(Book.ISBN, bookDetail.getIsbn());
		values.put(Book.PUBLICATION, bookDetail.getPublishDate());
		values.put(Book.PATH, bookDetail.getPath());
		values.put(Book.RESOURCE_PATH, bookDetail.getResourcePath());
		values.put(Book.IS_READING, bookDetail.getIsReading());
		long result = database.insert(Book.TABLE, null, values);
		Log.d("Update Result:", "=" + result);
		return result;
	}

	public long update(BookDetail bookDetail) {
		ContentValues values = new ContentValues();
		values.put(Book.PRODUCT_ID, bookDetail.getProductId());
		values.put(Book.NAME, bookDetail.getName());
		values.put(Book.AUTHOR, bookDetail.getAuthor());
		values.put(Book.DESC, bookDetail.getDescription());
		values.put(Book.IMAGE, bookDetail.getImage());
		values.put(Book.USER_ID, bookDetail.getUserId());
		values.put(Book.PRICE, bookDetail.getPrice());
		values.put(Book.ISBN, bookDetail.getIsbn());
		values.put(Book.PUBLICATION, bookDetail.getPublishDate());
		values.put(Book.PATH, bookDetail.getPath());
		values.put(Book.RESOURCE_PATH, bookDetail.getResourcePath());
		values.put(Book.IS_READING, bookDetail.getIsReading());

		long result = database.update(Book.TABLE, values, WHERE_ID_EQUALS,
				new String[] { String.valueOf(bookDetail.getId()) });
		Log.d("Update Result:", "=" + result);
		return result;

	}

	public long updateBookDetail(BookDetail bookDetail) {
		long count = 0;
		String[] projection = { Book._ID };
		Cursor cursor = database.query(Book.TABLE, // The table to query
				projection, // The columns to return
				Book.PRODUCT_ID + " =? AND " + Book.USER_ID + " =?", // The
																		// columns
																		// for
																		// the
																		// WHERE
																		// clause
				new String[] { String.valueOf(bookDetail.getProductId()),
						String.valueOf(bookDetail.getUserId()) }, // The values
																	// for the
																	// WHERE
				// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		if (cursor != null && cursor.moveToFirst()) {
			bookDetail.setId(cursor.getLong(cursor
					.getColumnIndexOrThrow(Book._ID)));
			count = update(bookDetail);

		} else {
			count = save(bookDetail);
		}

		return count;

	}

	public int deleteook(BookDetail bookDetail) {
		return database.delete(Book.TABLE, WHERE_ID_EQUALS,
				new String[] { String.valueOf(bookDetail.getId()) });
	}

	public List<BookDetail> getAllBooks(String id) {
		List<BookDetail> listBookDetails = new ArrayList<BookDetail>();
		// hp = new HashMap();
		String[] projection = { Book._ID, Book.PRODUCT_ID, Book.NAME,
				Book.AUTHOR, Book.DESC, Book.IMAGE, Book.USER_ID, Book.PRICE,
				Book.USER_ID, Book.ISBN, Book.PUBLICATION, Book.PATH,
				Book.IS_READING };

		String sortOrder = Book._ID + " ASC";

		Cursor cursor = database.query(Book.TABLE, // The table to query
				projection, // The columns to return
				Book.USER_ID + "= ?", // The columns for the WHERE clause
				new String[] { id }, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			BookDetail bookDetail = new BookDetail();
			bookDetail.setId(cursor.getLong(cursor
					.getColumnIndexOrThrow(Book._ID)));
			bookDetail.setProductId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRODUCT_ID)));
			bookDetail.setName(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.NAME)));
			bookDetail.setAuthor(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.AUTHOR)));
			bookDetail.setDescription(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.DESC)));
			bookDetail.setImage(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IMAGE)));
			bookDetail.setUserId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.USER_ID)));
			bookDetail.setPrice(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRICE)));
			bookDetail.setIsbn(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.ISBN)));
			bookDetail.setPublishDate(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PUBLICATION)));
			bookDetail.setPath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PATH)));
			int isReadingStatus = cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IS_READING)) == null ? 0
					: Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(Book.IS_READING)));
			bookDetail.setIsReading(isReadingStatus);
			listBookDetails.add(bookDetail);
			cursor.moveToNext();

		}
		return listBookDetails;
	}

	public List<BookDetail> getCurrentReadingBooks(String id) {
		List<BookDetail> listBookDetails = new ArrayList<BookDetail>();
		// hp = new HashMap();
		String[] projection = { Book._ID, Book.PRODUCT_ID, Book.NAME,
				Book.AUTHOR, Book.DESC, Book.IMAGE, Book.USER_ID, Book.PRICE,
				Book.USER_ID, Book.ISBN, Book.PUBLICATION, Book.PATH,
				Book.RESOURCE_PATH, Book.IS_READING };

		String sortOrder = Book._ID + " DESC";

		Cursor cursor = database.query(Book.TABLE, // The table to query
				projection, // The columns to return
				Book.USER_ID + "= ? AND " + Book.IS_READING + "=?", // The
																	// columns
																	// for the
																	// WHERE
																	// clause
				new String[] { id, String.valueOf(1) }, // The values for the
														// WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			BookDetail bookDetail = new BookDetail();
			bookDetail.setId(cursor.getLong(cursor
					.getColumnIndexOrThrow(Book._ID)));
			bookDetail.setProductId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRODUCT_ID)));
			bookDetail.setName(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.NAME)));
			bookDetail.setAuthor(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.AUTHOR)));
			bookDetail.setDescription(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.DESC)));
			bookDetail.setImage(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IMAGE)));
			bookDetail.setUserId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.USER_ID)));
			bookDetail.setPrice(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRICE)));
			bookDetail.setIsbn(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.ISBN)));
			bookDetail.setPublishDate(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PUBLICATION)));
			bookDetail.setPath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PATH)));
			bookDetail.setResourcePath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.RESOURCE_PATH)));
			int isReadingStatus = cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IS_READING)) == null ? 0
					: Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(Book.IS_READING)));
			bookDetail.setIsReading(isReadingStatus);
			listBookDetails.add(bookDetail);
			cursor.moveToNext();

		}
		return listBookDetails;
	}

	public BookDetail getUserBookDetail(String userId, String producetId) {

		BookDetail bookDetail = new BookDetail();
		String[] projection = { Book._ID, Book.PRODUCT_ID, Book.NAME,
				Book.AUTHOR, Book.DESC, Book.IMAGE, Book.USER_ID, Book.PRICE,
				Book.USER_ID, Book.ISBN, Book.PUBLICATION, Book.PATH,
				Book.RESOURCE_PATH, Book.IS_READING };

		Cursor cursor = database.query(Book.TABLE, // The table to query
				projection, // The columns to return
				Book.PRODUCT_ID + " =? AND " + Book.USER_ID + " =?", // THE COLUMNS FOR THE WHERE CLAUSE
				
				new String[] { producetId, userId }, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		if (cursor != null && cursor.moveToFirst()) {
			bookDetail.setId(cursor.getLong(cursor
					.getColumnIndexOrThrow(Book._ID)));
			bookDetail.setId(cursor.getLong(cursor
					.getColumnIndexOrThrow(Book._ID)));
			bookDetail.setProductId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRODUCT_ID)));
			bookDetail.setName(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.NAME)));
			bookDetail.setAuthor(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.AUTHOR)));
			bookDetail.setDescription(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.DESC)));
			bookDetail.setImage(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IMAGE)));
			bookDetail.setUserId(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.USER_ID)));
			bookDetail.setPrice(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PRICE)));
			bookDetail.setIsbn(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.ISBN)));
			bookDetail.setPublishDate(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PUBLICATION)));
			bookDetail.setPath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.PATH)));
			bookDetail.setResourcePath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.RESOURCE_PATH)));
			bookDetail.setResourcePath(cursor.getString(cursor
					.getColumnIndexOrThrow(Book.RESOURCE_PATH)));
			int isReadingStatus = cursor.getString(cursor
					.getColumnIndexOrThrow(Book.IS_READING)) == null ? 0
					: Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(Book.IS_READING)));
			bookDetail.setIsReading(isReadingStatus);

		}

		return bookDetail;

	}

	public boolean saveUserBookList(List<BookDetail> listUserBooks) {

		for (int i = 0; i < listUserBooks.size(); i++) {
			if (!isUserBookExist(listUserBooks.get(i))) {
				save(listUserBooks.get(i));
			} else {

			}

		}

		return true;

	}

	public boolean isUserBookExist(BookDetail bookDetail) {

		boolean status = false;
		String[] projection = { Book._ID };

		Cursor cursor = database.query(Book.TABLE, // The table to query
				projection, // The columns to return
				Book.PRODUCT_ID + " =? AND " + Book.USER_ID + " =?", // The
																		// columns
																		// for
																		// the
																		// WHERE
																		// clause
				new String[] { String.valueOf(bookDetail.getProductId()),
						String.valueOf(bookDetail.getUserId()) }, // The values
																	// for the
																	// WHERE
				// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		if (cursor != null && cursor.moveToFirst()) {
			status = true;
		}
		return status;

	}

}
